# La récursivité et Mr Python

##PS: toute resemblance avec du python est le pur hasard... ou pas.

Il est toujours bon de revenir au base. L’algorithme de la récursivité en fait partie. 
Mais il peut aussi être considérer comme un genre de coding dojo prototype.L'objectif est de lire l'ensemble des répertoires et sous-répertoire : un classique. 

## Definition : de quoi parle t’on ?
une méthode récursive est avant tout « une méthode pour retrouver sont chemins ». 

## Un exemple simple ? Afficher des nombres de 1 à 10.

La solution la plus simple est évidament une itération (une boucle) de 1 à 10. Deux lignes de code est c’est terminé.

    for i in range(10):
        print ('nombre: ',i+1);

Tout va bien tant que l’espace de travail est connu et déterminé (1 à 10).

Avec la récusivité, on a plus besoin de connaître les limites.Elles sont remplacé par une contrainte ou encore une condition qui determine la répétition de la méthode.

    def recusiveNombre(incNombre):
        print(incNombre);
        incNombre+= 1;
        if incNombre > 10:
            return;
        recusiveNombre(incNombre);


## Et pour les répertoires ?

Dans toute méthode, la premier chose est de définir la méthode principale

    monRepertoire = os.getcwd();
    contenuRepertoire =  os.listdir(monRepertoire);
    for item in contenuRepertoire:
        if os.path.isdir(item):
            print(item);

Elle va permetre de créer une fonction récursive

    def listeRepertoires(monRepertoire,path):
        contenuRepertoire =  os.listdir(monRepertoire);
        for item in contenuRepertoire:
            if os.path.isdir(f"{monRepertoire}\{item}"):            
                print(path+item);
                monRepertoireNew = f"{monRepertoire}\{item}";
                path+="--";
                listeRepertoires(monRepertoireNew,path); 
        return;

Chaque fois qu’un répertoire est trouvé, elle va se « rappeler » pour lister les sous-répertoires de celui-ci et ainsi de suite.

Je pense que ceci est avant tout possible par le fait qu’une variable locale  n’a d’existence que dans son bloc. Elle ne peuvent être atteinte que dans leur portée.

Bon coding Dojo !


